import numpy as np
import matplotlib.pyplot as plt
from mpyfit import fit
import pdb

def boxcar(x,a):
    """
    Function will produce a boxcar/tophat given a set of parameters

    Input
    -----
    x = independent values
    a = a list of background value, center, width, and the height of
        the bottom/top of the boxcar/tophat

    Output
    ------
    f = a set of corresponding dependent values of the boxcar function
    """

    f = a[0]+np.zeros(x.size)                    # Setting background
    lhs = np.abs(x - (a[1] - a[2]/2.)).argmin()  # Finding left edge
    rhs = np.abs(x - (a[1]+a[2]/2.)).argmin()    # Finding right edge
    f[lhs:rhs+1] = a[3]                          # Setting bottom/top

    return f

def min_boxcar(a,args):
    """
    Finds the absolute difference between the data and fitted boxcar

    Input
    -----
    a = parameters to feed to the boxcar function
    args = a tuple of the dependent and independent values

    Output
    ------
    The absolute difference between the dependent values and the boxcar
    function specified by a.
    """
    
    x,f = args                      # Unpacking dependent/independent vals
    return np.abs(f - boxcar(x,a))

def fit_transit(x,f):
    """
    Fits a transit with a boxcar function

    Input
    -----
    x,f = time and relative flux or flux values

    Output
    ------
    the transit depth, and the fitted boxcar
    """

    # background, center, width, K
    guess = [f.max(), x[f.argmin()], x.ptp()/4., f.min()]
    parameters, results = fit(min_boxcar, guess, (x,f))

    return parameters[-1]/parameters[0], boxcar(x,parameters)

# Read in data
t, dF = np.genfromtxt('transit.dat', delimiter=',', dtype=float).T

# Fit transit
K, DF = fit_transit(t,dF)

# Overplot fit
plt.plot(t,dF)
plt.plot(t,DF)

print("Transit depth: {}".format(1- K))
plt.show()

