ASTR_3800 - Introduction to Scientific Data Analysis and Computing
==================================================================

This repository will function as the website for the Fall 2015 class of ASTR 3800. Under "Source", you will find any notes associated with the class, help pages for various installations and tools we will use throughout the course, class projects and solutions, and the syllabus.

Questions regarding the use of this site can be directed to the TA, Baylee Bordwell, at:
baylee.bordwell@colorado.edu

Questions regarding the course can be directed to Baylee, or to the instructor, Mark Rast, at:
mark.rast@lasp.colorado.edu