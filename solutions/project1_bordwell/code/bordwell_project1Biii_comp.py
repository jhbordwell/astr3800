#!/usr/bin/python
import numpy as np
from saverestore import *
import pdb

def chi2(test,exp_fn):
    """
    This function will perform a general Pearson's chi-squared test on a 
    given dataset ([test]), calculated the expected values from a lambda
    function given as [exp_fn].
    """
    
    exp = exp_fn(test)
    return ((test-exp)**2/exp).sum()



# B.iii.a ----------------------------------------------------------------------------------
# Expected PDF should be a uniform distribution
def frequency_test(filename,b1,b2,b3):
    """
    This function will perform a frequency test to see if each number comes up 
    as often as it would be expected to given a uniform distribution (same number
    of numbers in each bin). Three different numbers of bins ([b1], [b2] and [b3]) 
    will be compared. Datasets will be restored from [filename].
    """
    data = np.array(restore(filename))

    # Creating the test as a lambda function
    ftest = lambda d, b: chi2( np.histogram(d,bins = b)[0],
                               lambda x: np.zeros(len(x))+1000/len(x))

    # Performing the test for each dataset and bin size
    x2 = [ [ftest(d,b) for b in [b1,b2,b3]] for d in data]

    return x2


# B.iii.b ----------------------------------------------------------------------------------
# MY INITIAL INTERPRETATION
# Expected PDF should have consecutive numbers 1/10 of the time
def serial_test1(filename, pair_prob):
    """
    This function will perform a serial test by finding the difference
    between each number and it's neighbor to the right and checking to 
    see if the difference is 0.1. Each number in the dataset given will be 
    rounded to 0.1 to make the comparison a little cleaner. The expected 
    number of neighboring pairs will be given as the input [pair_prob], and
    datasets will be restored from [filename].
    """
    data = np.array(restore(filename))

    x2 = []
    for i,d in enumerate(data):
        # Creating a boolean array based on whether or not a pair was present
        pairs = (d[1:].round(1) - d[:-1].round(1)) == 0.1

        # Making a histogram of that array
        hist, dummy = np.histogram(pairs, bins = 2)

        x2.append(chi2(hist/999., lambda x: np.array([1-pair_prob,pair_prob])))

    return x2


# MY INTERPRETATION AFTER A LONG CONVERSATION WITH MARK
# For one number to be in a bin, and have its neighbor in the next, there is a 1/100 probability
def serial_test2(filename, n_bins, pair_prob):
    """
    This function will perform a serial test by finding the difference
    between each number and it's neighbor to the right and checking to 
    see if the difference is one bin width, and then seeing how often this 
    is true for each bin. The number of bins will be given as [n_bins],
    the expected number of neighboring pairs will be given as the input 
    [pair_prob], and datasets will be restored from [filename].
    """

    data = restore(filename)

    x2 = []
    for i,d in enumerate(data):
        # Making a histogram of that array
        hist, patches = np.histogram(d, bins = 10)

        # Finding which bin each number is in
        locs = np.zeros(d.size)
        for j in range(1,patches.size-1):
            locs += (d>patches[j]) * (d<=patches[j+1]) * j

        # Finding how far away the bin their neighbor is in is
        diffs = locs[1:] - locs[:-1]

        # Making a distribution of the number of pairs in each bin
        dist = []
        for j in range(patches.size):
            dist.append( ((locs[:-1] == j)*(diffs == 1)).sum() )
        dist = np.array(dist)

        # Performing the chi square test
        x2.append( chi2( dist/float(len(diffs)),
                         lambda x: np.zeros(len(x)) + pair_prob ) )

    return x2


# Running tests and reporting results --------------------------------------------------------
def run_test(test, files, report, *test_args):
    """
    This function will run the tests written above taking in the function [test],
    the files the test needs to be run on [files], the function that will generate
    the report string [report], which will have inputs in the format 
    (filename, trial number, *results), and the additional arguments to the test 
    (besides the filename) as the tuple [test_args].
    """
    for f in files:
        # Concatenating the filename to the list of test arguments
        args = [f] + list(test_args)

        # Calling the test function
        result = test(*tuple(args))

        # Printing the results
        for i,r in enumerate(result):
            if type(r) != list: r = [r]
            print( report(*tuple( [f]+[i]+r )) )

            
            
# This will format a string to report the results cleanly
state = lambda *x: 'Results for {}, trial {}, bins = 10, 40 and 100: {}, {}, {}'.format(*x)

print('\nFrequency Test:')    
run_test(frequency_test,
         ['../data/bordwell_project1Ai.dat',
          '../data/bordwell_project1Aii.dat',
          '../data/bordwell_project1Aiii.dat'],
         state,
         *(10,40,100))


# This will format a string to report the results cleanly
state2 = lambda *x: 'Results for {} dataset, trial {}, width = 0.1: {}'.format(*x)

print('\nSerial Test (version 1):')
run_test(serial_test1,
         ['../data/bordwell_project1Ai.dat',
          '../data/bordwell_project1Aii.dat',
          '../data/bordwell_project1Aiii.dat'],
         state2,
         *(0.1,))

print('\nSerial Test (version 2):')
run_test(serial_test1,
         ['../data/bordwell_project1Ai.dat',
          '../data/bordwell_project1Aii.dat',
          '../data/bordwell_project1Aiii.dat'],
         state2,
         *(10,0.01,))
