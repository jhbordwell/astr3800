from saverestore import *

# A.i ------------------------------------------------------------------

# To create a sequence of 1000 random numbers, I tried to be as lazy as
# possible, and used the following code:

def human_numgen(size):
    """
    This function will generate a list of [size] random numbers using a human 
    number generator (a.k.a. the user).
    """
    
    list = []

    # a while loop ensures that I will get up to size numbers
    while len(list) < size:
        
        # the try/except statement ensures that I can mess up my button
        # mashing without having to start over
        try:
            for i in range(size):
                # Between [0,63], normalized to 64
                num = ( input("") % 63 ) / 64.
                list.append(num)
        except:
            pass

    return list


# This resulted, with a lot of keyboard mashing, in a relatively uniform
# distribution, which was somewhat satisfying.
list1 = human_numgen(1000)
list2 = human_numgen(1000)
list3 = human_numgen(1000)

# To create my data file, I used:
save('../data/bordwell_project1Ai.dat',
     ('list1','list2','list3'),
     (list1,list2,list3))
