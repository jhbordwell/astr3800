#!/usr/bin/python
import numpy as np
from saverestore import *
    
# A.ii ------------------------------------------------------------------
# Once again aiming to be as lazy as possible...
list1, list2, list3 = np.random.uniform(size=3000).reshape((3,-1))

save('../data/bordwell_project1Aii.dat',
     ('list1','list2','list3'),
     (list1,list2,list3))
