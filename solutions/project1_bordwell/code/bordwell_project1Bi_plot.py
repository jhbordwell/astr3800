#!/usr/bin/python
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from saverestore import *

# Setting the font size so things don't run over each other
font = {'size': 10}
matplotlib.rc('font',**font)


def random_time_series(filenames, savename):
    """
    This function will plot each random dataset as a time series,
    with labels and titles as requested by the assignment
    """
    # Setting the titles to be consistent with the assignment
    titles = ['a','b','c','d','e','f','g','h','i']

    # Setting up to make one giant figure with many plots
    fig, ax = plt.subplots(3,3,figsize=(12,12))
    
    for j,f in enumerate(filenames):
        # Reading in the data
        data = restore(f)

        for i,d in enumerate(data):
            ax[j][i].plot(d,c='r')
            ax[j][i].plot(d,'bo',markersize=2)

            ax[j][i].set_xlabel('Iteration')
            ax[j][i].set_ylabel('Random value')
            ax[j][i].set_title(titles[j*3+i])

    fig.tight_layout() # Makes the plots not run into each other
    fig.savefig(savename)

    
# Running the function on my datasets
random_time_series(['../data/bordwell_project1Ai.dat',
                    '../data/bordwell_project1Aii.dat',
                    '../data/bordwell_project1Aiii.dat'], \
                   '../figures/bordwell_project1Bi.png')

