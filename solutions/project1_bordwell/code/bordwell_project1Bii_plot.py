#!/usr/bin/python
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from saverestore import *

# Setting the font size so things don't run over each other
font = {'size': 10}
matplotlib.rc('font',**font)


def single_hist(ax, plot, data, width1, width2, title):
    """
    This function will plot a random dataset as 1 histograms of 1 bin width,
    with labels and titles as requested by the assignment
    """
    ax[plot].hist(data, bins = 1/width1)
    ax[plot].hist(data, bins = 1/width2, edgecolor='r', facecolor=None)
    ax[plot].set_xlabel('Random value')
    ax[plot].set_ylabel('Occurences')
    ax[plot].set_title(title)

    
def random_hist(ax, filenames, width1, width2, titles):
    """
    This function will plot all the datasets in a file as
    with labels and titles as requested by the assignment
    """
    
    for i, a in enumerate(ax):
        # Reading in the data
        data = np.array(restore(filenames[i]))
        for j, d in enumerate(data):
            single_hist(a,j,d,
                        width1,width2,titles[i])

        # Plotting the combined plot
        single_hist(a,j+1,data.flatten(),
                    width1,width2,titles[i])
    

# Setting up to make a figure with multiple plots
fig, ax = plt.subplots(3,4, figsize=(20,10))

# Running the function on all of the datasets
random_hist(ax,
            ['../data/bordwell_project1Ai.dat',
             '../data/bordwell_project1Aii.dat',
             '../data/bordwell_project1Aiii.dat'],
            0.1, 0.025,
            ['Human-gen', 'Numpy-gen', 'JvN-gen'])

fig.tight_layout() # Makes the plots not run into one another
fig.savefig('../figures/bordwell_project1Bii.png')
