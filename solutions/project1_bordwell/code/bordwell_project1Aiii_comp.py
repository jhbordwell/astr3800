#!/usr/bin/python
import numpy as np
from saverestore import *
    
# A.iii -----------------------------------------------------------------

def JvN_numgen(seed,size):
    """
    This code will generate random numbers using the first John von Neumann
    pseudo-random number technique. It will:

    - Take in a 4 digit number as a seed
    - Square it
    - Take the middle four numbers
    - Repeat

    All numbers will be normalized by 10000.

    INPUT:
    seed = the starting four digit number
    size = the number of times to repeat this procedure
    """
    
    lyst, old_seed = [], 0
    for i in range(1000):
        # Converting the number into a string makes it simple to grab the middle 4
        seed_str = "{:d}".format(seed**2)
        l = len(seed_str)

        seed = int(seed_str[l/2-3 : l/2+4])
        if seed == old_seed: seed = int((seed + 424242) % 1e7)
        old_seed = seed
        lyst.append(seed / 1e7)

    return np.array(lyst)


# Obtaining my pseudorandom number sets
list1 = JvN_numgen(666111,1000)
list2 = JvN_numgen(322434,1000)
list3 = JvN_numgen(133113,1000)

save('../data/bordwell_project1Aiii.dat',
     ('list1','list2','list3'),
     (list1,list2,list3))
