import matplotlib.pyplot as plt
from astropy.io import fits

# Reading in the data and setting it up to be shown
data = fits.getdata('../data/frame-g-000094-1-0131.fits')
data -= data.min() # Getting rid of negative numbers
data += 0.001 # Providing a vertical offset away from zero

# Getting the edges of the image in ra, dec
ra, dec = 354.473181333, -0.929952240169
y, x = data.shape
deg_per_pix = 0.396 / 60 / 60
ra_x = x * deg_per_pix
dec_y = y * deg_per_pix

# Plotting the image
plt.imshow(np.log10(data),cmap='gray',
           extent = (ra - ra_x/2., ra + ra_x/2.,
                     dec - dec_y/2., dec + dec_y/2.),
           origin = 'lower')
plt.xlabel('RA [deg]')
plt.ylabel('DEC [deg]')

# Making it so my axes don't have the weird exponential offsets
plt.gca().get_xaxis().get_major_formatter().set_useOffset(False)

plt.savefig('../figures/bordwell_project2Ai.png')
