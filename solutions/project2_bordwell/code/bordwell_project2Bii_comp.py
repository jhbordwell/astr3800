import numpy as np
from saverestore import *


def nearest_neighbors(x,y):
    """
    Given two sets of positions [x] and [y], this function will find the 
    nearest, non-zero 2 dimensional distance between each point and its 
    neighbors using the distance formula.
    """

    # Finding the distances
    diff = lambda a: a.reshape((a.size,1)) - a.reshape((1,a.size))
    nearest = ( diff(x)**2+diff(y)**2 )

    # Masking out the positions where each point was compared to itself
    nearest[np.where(nearest == 0)] = nearest.max()
    nearest = nearest.min(0)**0.5
    return nearest


def make_ideal_histogram(samp, low=None, high=None):
    """
    This function will pick the optimal bin sizes for a histogram, i.e.
    it will seek to have as many bins containing 5 or fewer samples as
    is possible without having an absurd number of bins by seeking to have a 
    mean bin fullness of 5 or less. It will operate upon a dataset [samp] and 
    range the bins between the values [low] and [high], which will be taken 
    from the dataset unless given.
    """
    # Getting the bin limits if not provided
    if not low: low = samp.min()
    if not high: high = samp.min()

    # This will provide all the left edges and one right edge for np.histogram
    bins = lambda x: np.linspace(low,high,x)

    # Looping until the mean number of items in a bin is <= 5
    d = samp.size / 10.
    bad_hist = True
    while bad_hist:
        d += 1
        hist, dummy = np.histogram(samp,bins = bins(d))
        bad_hist = hist.mean() > 5

    return bins(d), hist


# Finding the nearest neighbors for the Sloan data
x,y,g = np.genfromtxt('../data/frame-g-000094-1-0131.csv',
                      delimiter=',',
                      dtype = float,
                      skip_header=1).T
nn = nearest_neighbors(x,y)

# Performing the same test for the random dataset
x_rand, y_rand = restore('../data/bordwell_project2Bi.dat')
nn_rand = nearest_neighbors(x_rand,y_rand)

# Saving the results
save('../data/bordwell_project2Bii.dat', ('Data NN', 'Random NN'), (nn, nn_rand))


# Getting the lower and upper bounds for the histogram bins
low = min([nn.min(),nn_rand.min()])
high = max([nn.max(),nn_rand.max()])

# Finding the distribution for the Sloan data and random dataset
bins, hist = make_ideal_histogram(nn,low=low,high=high)
bins_rand, hist_rand = make_ideal_histogram(nn_rand,low=low,high=high)

# Ensuring that each distribution uses the same bins
if bins.size > bins_rand.size:
    hist_rand, dummy = np.histogram(nn_rand,bins=bins)
else:
    hist, dummy = np.histogram(nn,bins=bins_rand)
    bins = bins_rand


# Performing the chi square test using the random dataset as the expected    
chi2 = ( (hist - hist_rand)**2 / hist_rand ).sum()

print("The X^2 value for the nearest neighbor distances with"+\
      " {} bins is {}".format(bins.size,chi2))


# Saving the histogram data for the plots
save('../data/bordwell_project2Bii_plot.dat',
     ('Bins', 'Data hist', 'Random hist'),
     (bins, hist, hist_rand))


      
