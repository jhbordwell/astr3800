import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
from bordwell_project2Aii_plot import scatter_plot
from saverestore import *

# Image domain and the length of the csv file - 1
x, y, n_stars = 2048, 1498, 332 
x_rand, y_rand = npr.random(n_stars) * x, npr.random(n_stars) * y

# Checking to see if all the pairs are unique
bad_set = True
while bad_set:
    diff = lambda a: a - a.reshape((1,a.size))
    inds = np.where((diff(x_rand) + diff(y_rand)) == 0)
    if any(inds[0]):
        y_rand = np.shuffle(y_rand)
    else:
        bad_set = False

# Plotting
scatter_plot(x_rand,y_rand)
plt.title('Artificial starfield')
plt.savefig('../figures/bordwell_project2Bi.png')

# Saving the random dataset
save('../data/bordwell_project2Bi.dat',
     ('x_rand','y_rand'),
     (x_rand,y_rand))

