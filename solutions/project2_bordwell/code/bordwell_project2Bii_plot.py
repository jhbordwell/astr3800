import numpy as np
import matplotlib.pyplot as plt
from saverestore import *

# Restoring the histogram data for the plots
bins, hist, hist_rand = restore('../data/bordwell_project2Bii_plot.dat')

# Plotting the Sloan results
fig, ax = plt.subplots(3,figsize=(6,9))
ax[0].bar(bins[:-1], hist, width = bins[1]-bins[0])
ax[0].set_title('Sloan Nearest Neighbors')
ax[0].set_ylabel('Occurences')
ax[0].set_ylim(0,hist.max())
ax[0].set_xlim(0,bins.max())

# Plotting the artificial starfield result on the same scale
ax[1].bar(bins[:-1], hist_rand, width = bins[1]-bins[0])
ax[1].set_title('Artificial Nearest Neighbors')
ax[1].set_xlabel('Nearest Neighbor distances')
ax[1].set_ylabel('Occurences')
ax[1].set_ylim(0,hist.max())
ax[1].set_xlim(0,bins.max())

# This plot is extra, I just tossed it in because I wanted to see it
ax[2].bar(bins[:-1], hist-hist_rand, width = bins[1]-bins[0])
ax[2].set_title('Nearest Neighbors Differences')
ax[2].set_xlabel('Nearest Neighbor distances')
ax[2].set_ylabel('Occurences')
ax[2].set_xlim(0,bins.max())

fig.tight_layout()
fig.savefig('../figures/bordwell_project2Bii.png')
