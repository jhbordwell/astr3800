import numpy as np
import matplotlib.pyplot as plt
import pdb

# Reading in the data and setting it up to be shown
data = np.genfromtxt('../data/frame-g-000094-1-0131.csv',
                     delimiter=',',
                     dtype = float,
                     skip_header=1).T

# Getting the intensities for part Aiii
intensity = 2.512**(data[2]-data[2].min())

def scatter_plot(x,y,size=100):
    # Plotting and scaling the size to avoid crazy point sizes
    plt.clf()
    plt.scatter(x,y,s=size)

    # Removing blank space in the plot
    plt.xlim(x.min(),x.max())
    plt.ylim(y.min(),y.max())

    plt.xlabel('X [pixels]')
    plt.ylabel('Y [pixels]')

    # Making it so my axes don't have the weird exponential offsets
    plt.gca().get_xaxis().get_major_formatter().set_useOffset(False)


scatter_plot(data[1],data[0])
plt.title('Sloan Data')
plt.savefig('../figures/bordwell_project2Aii.png')
    
scatter_plot(data[1],data[0],
             size= intensity**2/intensity.ptp()**2 * 1000)
plt.title('Sloan Data - size proportional to intensity')
plt.savefig('../figures/bordwell_project2Aiii.png')
