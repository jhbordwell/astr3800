import sys
import time
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import my
#
plt.ion()
#
print 'rast_project1Bii_plot.py'
#
#
#Project1B(ii):  Load random time series from data files and plot histograms.
#
case=3
#
if (case == 1):
    quist=my.restore('quist_project1Ai.dat')
    hinton=my.restore('hinton_project1Ai.dat')
    rast=my.restore('rast_project1Ai.dat')
    numseries=3
    numelements=1000
    random=np.zeros([numseries,numelements])
    random[0,:]=quist
    random[1,:]=hinton
    random[2,:]=rast
if (case == 2):
    random=my.restore('rast_project1Aii.dat')
    numseries=random.shape[0]
    numelements=random.shape[1]
if (case == 3):
    random=my.restore('rast_project1Aiii.dat')
    numseries=random.shape[0]
    numelements=random.shape[1]
#
plt.close()
#
fullfig=plt.figure(0,figsize=(7.5,9))
if (case == 1): fullfig.suptitle('Project1B(ii): Using human brain random number generator',fontsize=14)
if (case == 2): fullfig.suptitle('Project1B(ii): Using numpy random number generator',fontsize=14)
if (case == 3): fullfig.suptitle('Project1B(ii): Using von Neumann random number algorithm',fontsize=14)
#
for n in range(1,numseries+2): 
    subfig=plt.subplot(2,2,n)
    if (n == numseries+1):
        hist1=plt.hist(random.flatten(),bins=10,normed=True,histtype='bar',color='0.75')
        hist2=plt.hist(random.flatten(),bins=40,normed=True,histtype='step',color='red',linewidth=1.25)
    else:
        hist1=plt.hist(random[n-1,:],bins=10,normed=True,histtype='bar',color='0.75')
        hist2=plt.hist(random[n-1,:],bins=40,normed=True,histtype='step',color='red',linewidth=1.25)
#
#    subfig.tick_params('both',length=20,width=2,which='major')
#    subfig.tick_params('both',length=10,width=1,which='minor')
    xmx=1.0
    plt.xlim([0.0,xmx])
    subfig.minorticks_on()
    xticklabel=['0.0',' ','0.2',' ','0.4',' ','0.6',' ','0.8',' ','1.0']
    plt.xticks(np.arange(0,1.1,0.1),xticklabel)
    plt.grid(which='major',axis='x')
    xminor=tck.MultipleLocator(0.025)
    subfig.xaxis.set_minor_locator(xminor)
#    if (case == 2): ymx=1.6
#    if (case == 3): ymx=3.5
    ymx=2.0
    plt.ylim([0.0,ymx])
    if (n == 2 or n == 4):
        subfig.yaxis.tick_right()
        subfig.yaxis.set_ticks_position('both')
    yticklabel=['0.0',' ','0.2',' ','0.4',' ','0.6',' ','0.8',' ','1.0',' ','1.2',' ','1.4',' ','1.6',' ','1.8',' ','2.0']
    plt.yticks(np.arange(0,2.1,0.1),yticklabel)
    yminor=tck.MultipleLocator(0.05)
    subfig.yaxis.set_minor_locator(yminor)
#
    if (n == 1): subtitletext='Sequence 1:'
    if (n == 2): subtitletext='Sequence 2:'
    if (n == 3): subtitletext='Sequence 3:'
    if (n == 4): subtitletext='All together:'
    plt.title(subtitletext,loc='left')
#
#    if (case == 1): abclabel=['(a)','(b)','(c)']
#    if (case == 2): abclabel=['(d)','(e)','(f)']
#    if (case == 3): abclabel=['(h)','(i)','(j)']
#    if (n == 1): plt.title(titletext)
#    plt.text(920,1.0,abclabel[n-1],style='italic',fontsize=18)
    if (n == 1 or n == 3): plt.text(-0.28,0.755,'P(V)',style='italic',fontsize=14)
    if (n == 2 or n == 4): plt.text(1.13,0.755,'P(V)',style='italic',fontsize=14)
    plt.text(0.4,-0.25,'Value',style='italic',fontsize=14)
#
plt.subplots_adjust(hspace=0.35,wspace=0.15)
#
plt.savefig('rast_project1Bii_case'+str(case)+'.ps',dpi=600,papertype='letter',format='ps',bbox_inches='tight')
#
print 'rast_project1Bi_plot.py'

