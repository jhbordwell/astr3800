import numpy as np
import matplotlib.pyplot as plt
#
import my 
#
print 'rast_project1Ai_comp.py'
#
#When using ipython use "%load_ext autoreload" and then "%autoreload 2" (% signifies a "magic" command -- it takes arguments on the command line  without function syntax) to automatically reload modules after editing OR use dreload (standing for dreload)each time you make a change.  To be absolutely sure that everything is new restart ipython.
#
#Arrays in Python are dynamically allocated memory. They can be fixed size or dynamic. These are illustrated with functions loadrandom1a and loadrandom1b in loadrandom1.py.
#test=my.loadrandom1a(3,63)
#print test
#test=my.loadrandom1b(3,63)
#print test
#
#Project1A(i):  Load random1 from keybaoard (if bload=True) and save it to file so that it does not have to be created again, or read it from previously stored file (if bload=False)
#
numseries1=3
numelements1=1000
high=63
#bload=True	#create random number sequnces
bload=False	#restore random number sequences previously created
if bload:
    numseries1=1
    random1=np.zeros([numseries1,numelements1],np.float64)
    for n in range(0,numseries1):
        random0=my.loadrandom1(numelements1,high)
        random1[n,:]=random0/np.float64(high+1)
        my.save('rast_project1Ai.dat',['random1'],[random1])
else:
    quist=my.restore('quist_project1Ai.dat')
    hinton=my.restore('hinton_project1Ai.dat')
    rast=my.restore('rast_project1Ai.dat')
    random1=np.zeros([numseries1,numelements1])
    random1[0,:]=quist
    random1[1,:]=hinton
    random1[2,:]=rast
#
print 'rast_project1Ai_comp.py'
