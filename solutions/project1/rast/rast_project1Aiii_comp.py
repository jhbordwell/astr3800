import sys
import time
import numpy as np
import matplotlib.pyplot as plt
#
import my
#
print 'rast_project1Aiii_comp.py'
#
#Project1A(iii):  Load random3 using von Neumann scheme
#
numseries3=3
numelements3=1000
#iload=True
iload=False
if iload:
    random3=np.zeros([numseries3,numelements3],np.float64)
    seed3=[421902,105923,316954]
    for n in range(0,numseries3):
        random0=np.zeros(numelements3,np.float64)
 	restart=True
	while restart:
            restart=False
            next=seed3[n]**2
	    print next
            for m in range(0,numelements3):
                s=str(next)
                if (np.float64(s) > 999): 
                    random0[m]=np.float64(s[3:9]) 
                else:
                    random0[m]=np.float64(s)
               # sys.exit()
                if (random0[m] == random0[m-1]):
                    restart=True
                    seed3[n]=seed3[n]+1
                    print 'break',n,m,random0[m-1],random0[m],'newseed: ',seed3[n]
                    time.sleep(1.0)
                    break   
                else:
#                    time.sleep(0.1)
	            next=int(random0[m]**2)
#                    print next
        random3[n,:]=random0/np.float64(1.0e06)
    my.save('rast_project1Aiii.dat',['random3'],[random3])
else:
    random3=my.restore('rast_project1Aiii.dat')
    numseries3=random3.shape[0]
    numelements3=random3.shape[1]
#
print 'rast_project1Aiii_comp.py'

