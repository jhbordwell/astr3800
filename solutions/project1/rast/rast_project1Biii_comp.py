import sys
import time
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
import scipy.stats as stats
import my
#
print 'rast_project1Biii_comp.py'
#
#Project1B(iii):  Load random time series from data files and evaluate randomness.
#
case=2
#
if (case == 1):
    quist=my.restore('quist_project1Ai.dat')
    hinton=my.restore('hinton_project1Ai.dat')
    rast=my.restore('rast_project1Ai.dat')
    numseries=3
    numelements=1000
    random=np.zeros([numseries,numelements])
    random[0,:]=quist
    random[1,:]=hinton
    random[2,:]=rast
if (case == 2):
    random=my.restore('rast_project1Aii.dat')
    numseries=random.shape[0]
    numelements=random.shape[1]
if (case == 3):
    random=my.restore('rast_project1Aiii.dat')
    numseries=random.shape[0]
    numelements=random.shape[1]
#
#Frequency Test:
#
freqchisq1=np.zeros([4],np.float64)
freqchisq2=np.zeros([4],np.float64)
freqchisq3=np.zeros([4],np.float64)
nbin1=10
nbin2=40
nbin3=100
freqscipychisq1=np.zeros([4],np.float64)
freqscipyp1=np.zeros([4],np.float64)
freqscipychisq2=np.zeros([4],np.float64)
freqscipyp2=np.zeros([4],np.float64)
freqscipychisq3=np.zeros([4],np.float64)
freqscipyp3=np.zeros([4],np.float64)
#
for n in range(1,np.int64(numseries)+2):
    if (n == np.int64(numseries)+1):
        hist1=np.histogram(random.flatten(),bins=nbin1)
        hist2=np.histogram(random.flatten(),bins=nbin2)
        hist3=np.histogram(random.flatten(),bins=nbin3)
        hist1=np.float64(hist1[0])
        hist2=np.float64(hist2[0])
        hist3=np.float64(hist3[0])
        freqchisq1[n-1]=np.sum(((hist1-np.float64(numseries)*np.float64(numelements)/np.float64(nbin1))**2)/(np.float64(numseries)*np.float64(numelements)/np.float64(nbin1)))
        freqchisq2[n-1]=np.sum(((hist2-np.float64(numseries)*np.float64(numelements)/np.float64(nbin2))**2)/(np.float64(numseries)*np.float64(numelements)/np.float64(nbin2)))
        freqchisq3[n-1]=np.sum(((hist3-np.float64(numseries)*np.float64(numelements)/np.float64(nbin3))**2)/(np.float64(numseries)*np.float64(numelements)/np.float64(nbin3)))
        tmp=stats.chisquare(hist1,f_exp=None)
        freqscipychisq1[n-1]=tmp[0]
        freqscipyp1[n-1]=tmp[1]
        tmp=stats.chisquare(hist2,f_exp=None)
        freqscipychisq2[n-1]=tmp[0]
        freqscipyp2[n-1]=tmp[1]
        tmp=stats.chisquare(hist3,f_exp=None)
        freqscipychisq3[n-1]=tmp[0]
        freqscipyp3[n-1]=tmp[1]
    else:
        hist1=np.histogram(random[n-1,:],bins=nbin1)
        hist2=np.histogram(random[n-1,:],bins=nbin2)
        hist3=np.histogram(random[n-1,:],bins=nbin3)
        hist1=np.float64(hist1[0])
        hist2=np.float64(hist2[0])
        hist3=np.float64(hist3[0])
        freqchisq1[n-1]=np.sum(((hist1-np.float64(numelements)/np.float64(nbin1))**2)/(np.float64(numelements)/np.float64(nbin1)))
        freqchisq2[n-1]=np.sum(((hist2-np.float64(numelements)/np.float64(nbin2))**2)/(np.float64(numelements)/np.float64(nbin2)))
        freqchisq3[n-1]=np.sum(((hist3-np.float64(numelements)/np.float64(nbin3))**2)/(np.float64(numelements)/np.float64(nbin3)))
        tmp=stats.chisquare(hist1,f_exp=None)
        freqscipychisq1[n-1]=tmp[0]
        freqscipyp1[n-1]=tmp[1]
        tmp=stats.chisquare(hist2,f_exp=None)
        freqscipychisq2[n-1]=tmp[0]
        freqscipyp2[n-1]=tmp[1]
        tmp=stats.chisquare(hist3,f_exp=None)
        freqscipychisq3[n-1]=tmp[0]
        freqscipyp3[n-1]=tmp[1]
#
print 'Chi-squared test of frequency:'
casename=['Human generated ','numpy random ','von Neumann ']
print ' '
print casename[case-1]+'(dof='+str(nbin1-1)+'): ',freqchisq1
print 'SciPy chi square value: ',freqscipychisq1
print 'Probability of a larger value of chi square; ',freqscipyp1
print ' '
print casename[case-1]+'(dof='+str(nbin2-1)+'): ',freqchisq2
print 'SciPy chi square value: ',freqscipychisq2
print 'Probability of a larger value of chi square; ',freqscipyp2
print ' '
print casename[case-1]+str(case)+'(dof='+str(nbin3-1)+'): ',freqchisq3
print 'SciPy chi square value: ',freqscipychisq3
print 'Probability of a larger value of chi square: ',freqscipyp3
print ' '
#
#Serial Test:
#
nbin=10
binvalue=np.arange(0.0,1.1,0.1)
neighborcount=np.zeros(nbin-1)
for n in range(0,numseries):
    rand=random[n,:]
    for m in range(0,nbin-1):
#        print m,binvalue[m],binvalue[m+1]
        lft=np.intersect1d(np.where(rand >= binvalue[m]),np.where(rand < binvalue[m+1]))
        rht=np.intersect1d(np.where(rand >= binvalue[m+1]),np.where(rand < binvalue[m+2]))
	nc=0
        for mm in range(0,len(lft)):
            wh=np.where(rht==lft[mm]+1)
            nc=nc+len(wh[0])
#            print nc
        neighborcount[m]=nc
#
    print neighborcount
    chisqneighbor=np.sum((neighborcount-10.)**2)/10.
    tmp=stats.chisquare(neighborcount,f_exp=10)
    print chisqneighbor,tmp[0]
    print tmp[1]
#
print 'rast_project1Biii_comp.py'
