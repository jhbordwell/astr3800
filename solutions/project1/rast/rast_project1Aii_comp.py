import sys
import time
import numpy as np
import matplotlib.pyplot as plt
#
import my
#
print 'rast_project1Aii_comp.py'
#
#Project1A(ii):  Load random2 using numpy random number generator
#
numseries2=3
numelements2=1000
#numelements2=1000000
seed2=[62659,32557,122188]
random2=np.zeros([numseries2,numelements2],np.float64)
for n in range(0,numseries2):
    np.random.seed(seed2[n])
    random0=np.random.uniform(size=(numelements2))
    random2[n,:]=random0
my.save('rast_project1Aii.dat',['random2'],[random2])
#
print 'rast_project1Aii_comp.py'
#
#NOTE: In python indexing can extend beyond the physical dimension of the array, but only extent of the actuall array is recovered.  This raises the difference between indexing s[3] or s[[3,5,7,9]] and slicing s[3:9] or s[3:*] or s[:,9]. In Python you slice between elements. A slice is not a range of indices, so something like s[[3:7,9] will not work.
