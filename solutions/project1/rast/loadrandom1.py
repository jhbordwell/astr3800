#define fixed size array and fill it with numbers input from keyboard
#num=total number of entries to be read from command line
#high=maximum value of positive integer entry
#
#import numpy as np
import my
#
def loadrandom1a(num,high):
    random1=np.zeros(num,np.float64)
    print 'Enter '+str(num)+' integers on interval [0,'+str(high)+']:'
    for n in range(0,num):
        numin=input()
        if (numin < 0) or (numin > high):
            invalid_input=True
            print 'Integer must lie in interval [0,'+str(high)+']:'
            while invalid_input:
                numin=input()
                if (numin >= 0) and (numin <= high):
                    invalid_input=False
                else:
                    print 'Integer must lie in interval [0,'+str(high)+']:'
        random1[n]=numin
        print random1[0:num]
#
#    print random1.dtype
    return random1
#
#
#define dynamic array and fill it
#num=total number of entries to be read from command line
#high=maximum value of positive integer entry
def loadrandom1b(num,high):
    random1=[]
    print 'Enter '+str(num)+' integers on interval [0,'+str(high)+']:'
    for n in range(0,num):
        numin=np.float64(input())
        if (numin < 0) or (numin > high):
            invalid_input=True
            print 'Integer must lie in interval [0,'+str(high)+']:'
            while invalid_input:
                numin=input()
                if (numin >= 0) and (numin <= high):
                    invalid_input=False
                else:
                    print 'Integer must lie in interval [0,'+str(high)+']:'
        random1.append(numin)
        print random1[0:num]
#
# Convert to a numpy arrray rather than a list (for performance, compactness, and functionality)
    random1=np.array(random1,np.float64)
#    print random1.dtype
    return random1
#
#
#define fixed size array and fill it with numbers input from keyboard
#num=total number of entries to be read from command line
#high=maximum value of positive integer entry
#
import numpy as np
#
def loadrandom1(num,high):
    random1=np.zeros(num,np.float64)
    print 'Enter '+str(num)+' integers on interval [0,'+str(high)+']:'
    for n in range(0,num):
        numin=input()
        if (numin < 0) or (numin > high):
            invalid_input=True
            print 'Integer must lie in interval [0,'+str(high)+']:'
            while invalid_input:
                numin=input()
                if (numin >= 0) and (numin <= high):
                    invalid_input=False
                else:
                    print 'Integer must lie in interval [0,'+str(high)+']:'
        random1[n]=numin
        print 'n= ',n
#
    return random1
