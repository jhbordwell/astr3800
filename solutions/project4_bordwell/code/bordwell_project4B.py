import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import chi2

from bordwell_project4A import fit_poly, fit_line

def process_datafile(name, columns, color, label,
                     skip_header= 1, alt_x=False):
    """
    This function reads in a datafile, corrects the recessional velocity
    to a different definition of redshift, and performs a linear fit 
    between the distance and this corrected velocity. The fit and data
    points are then plotted in the same color. Basically, this function
    does part B of project 4 and isn't very general (sorry all).

    Input:
    name = The filename to be read in
    columns = The columns of the file to be used, as a tuple
    color = The color to plot the points and line in
    label = The label to stick on the line/points
    skip_header = An optional keyword to skip header lines in the file. 
                  Default: 1
    alt_x = An alternative range of x-values to plot the fit over, optional.

    Output:
    (None)

    """

    # Reading in the data file
    d,v = np.genfromtxt(name, usecols=columns, dtype=float,
                        delimiter=',',skip_header = skip_header).T

    # Correcting the velocities
    c = 2.998e5 # km/s, speed of light
    v = c * ( ( (1+v/c) / (1-v/c) )**0.5 - 1) # Correcting velocity

    # Fitting a line to the data
    v_fit, unc_v, A, unc_A = fit_line(d,v)
    print("Fitted slope: {}+-{}\n".format(A[1],unc_A[1]))    

    # Plotting the data and fitted line
    plt.plot(d,v, color=color, marker='o', linestyle='none')
    plt.plot(alt_x, alt_x*A[1] + A[0], color=color, zorder=10, label=label)
    

# Finding the range which the full line plots should cover 
mx = np.genfromtxt('../data/ned4dlevel5.csv',delimiter=',',
                   dtype=float, usecols=(3,), skip_header=2).max()
alt_x = np.arange(0,mx)

# Fitting and plotting all the datasets
process_datafile('../data/ned1dlevel5.csv', (3,11), 'r', 'Near galaxies',
                 skip_header=2, alt_x = alt_x)
process_datafile('../data/ned4dlevel5.csv', (3,10), 'b', 'Far galaxies',
                 skip_header=2, alt_x = alt_x)
process_datafile('../data/hubbleoriginal.csv', (1,2), 'k', 'Hubble galaxies',
                 alt_x = alt_x)


# Extra -----------------------------------------------------------------------
# Reading in and combing the two modern datasets
dn,vn, note_n = np.genfromtxt('../data/ned1dlevel5.csv', usecols=(3,11,4), dtype=str,
                       delimiter=',',skip_header = 2).T
df,vf, note_f = np.genfromtxt('../data/ned4dlevel5.csv', usecols=(3,10,4), dtype=str,
                       delimiter=',',skip_header = 2).T
d = np.array(np.hstack((dn,df)), dtype=float)
v = np.array(np.hstack((vn,vf)), dtype=float)
notes = np.hstack((note_n, note_f))

# Correcting the velocities
c = 2.998e5 # km/s, speed of light
v = c * ( ( (1+v/c) / (1-v/c) )**0.5 - 1) # Correcting velocity

# Fitting a line
v_fit, unc_v, A, unc_A = fit_line(d,v)
print("Fitted slope: {}+-{}\n".format(A[1],unc_A[1]))    

# Plotting
plt.plot(alt_x, alt_x*A[1] + A[0], color='m', zorder=10, label='All modern galaxies')
# -----------------------------------------------------------------------------


# Doing the rest of the plotting stuff
plt.xlabel('Distance (Mpc)')
plt.ylabel('Recessional velocity (km/s)')
plt.xlim(d.min(),d.max())
plt.ylim(v.min(),v.max())
plt.title("Fits to Galaxy Data")
plt.legend(loc='lower right', fontsize=10)
plt.savefig('../figures/bordwell_project4B.png')


# Extra ----------------------------------------------------------------------
# Finding the Type Ia supernova
v /= c
inds = np.where(notes == 'SNIa')


plt.clf()
plt.plot(v[inds], d[inds], 'ro')
plt.ylabel('Distance (Mpc)')
plt.xlabel('Redshift, z')
plt.title("Type Ia supernova")
plt.savefig('../figures/bordwell_project4Bx.png')
# -----------------------------------------------------------------------------

