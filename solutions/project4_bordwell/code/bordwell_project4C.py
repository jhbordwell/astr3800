import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import chi2


def fit_correction(x,y,r,d):
    """
    This function will fit Hubble's correction to his original dataset.
    This correction takes the form:

    v = Ho*r + Xcos(a)cos(d) + Ysin(a)cos(d) + Zsin(d)

    where v is the recessional velocity of a galaxy, r is the distance 
    to the galaxy, a is the right ascension and d is the declination.


    Input:
    x = The distances to a set of galaxies
    y = The corresponding recessional velocities
    r = The corresponding right ascensions
    d = The corresponding declinations

    Output:
    fit = The linear fit to the corrected velocities and distances
    correction = The correction to the original velocities provided

    ---------------------------------------------------------------
    Example:
    V, D, ra, dec = np.genfromtxt(datafile).T
    fit, dV = fit_correction(D,V,ra,dec)

    """

    # Generating the X matrix for the correction model above
    X = np.vstack(( x,
                    np.cos(r)*np.cos(d),
                    np.sin(r)*np.cos(d),
                    np.sin(d) ))

    # Fitting and reporting the coefficients of the fit
    A = np.dot( np.linalg.inv(np.dot(X,X.T)), np.dot(y,X.T) )
    print("Ho: {}, X: {}, Y: {}, Z: {}".format(*tuple(A)))

    # Finding the fit and correction
    fit = x * A[0]
    correction = np.dot(A[1:],X[1:])

    # Finding and reporting the chi square and p values
    x2 = ( (y-fit)**2 / np.abs(fit) ).sum()
    p = 1 - chi2.cdf(x2, x.size - 2)
    print("X2: {}, p-val: {}".format(x2,p))
    
    
    return fit, correction


# Reading in the original hubble data
D,v,r,d = np.genfromtxt('../data/hubbleoriginal.csv',delimiter=',',
                        dtype=float,skip_header=1,usecols=(1,2,3,4)).T

# Converting the ra and dec to radians
r *= 15*np.pi/180.
d *= np.pi/180.

# Performing the fit
fit, dv = fit_correction(D,v,r,d)

# Plotting
plt.plot(D,v-dv,'ko')
plt.plot(D,fit,color='r')
plt.xlabel('Distance (Mpc)')
plt.ylabel('Recessional velocity (km/s)')
plt.title("Fits to Hubble's Original Data (corrected)")
plt.savefig('../figures/bordwell_project4C.png')
