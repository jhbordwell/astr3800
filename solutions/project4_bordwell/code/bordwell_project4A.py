import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import chi2
import pdb

def fit_poly(x,y,n, no_offset=False):
    """
    This function will fit some degree of polynomial to a set of data.

    Input:
    x = Independent values
    y = Dependent values
    n = Degree of polynomial
    no_offset = A keyword to turn off fitting a y-intercept (i.e. force
                the fit to go through the origin)

    Output:
    A = Coefficients of the fit, from lowest to highest degree
    np.dot(A,X) = The fitted line
    """

    # Creating the X matrix
    X = np.vstack([x**i for i in range(n+1)])
    if no_offset: X = X[1:] # Adjusts to not fitting a y-intercept

    # Fitting a polynomial to the data
    A = np.dot( np.linalg.inv(np.dot(X,X.T)), np.dot(y, X.T) )

    return A, np.dot(A,X)
 

def fit_line(x,y, no_offset=False):
    """
    This function will fit a line to a set of data, calculate the
    uncertainties of the fit and calculate a chi square and p value.
    
    Input:
    x = Independent values
    y = Dependent values
    no_offset = A keyword to turn off fitting a y-intercept (i.e. force
                the fit to go through the origin)

    Output:
    fit = The fitted line
    sig_y = The uncertainty in the fitted line
    A = The coefficients of the fit, from lowest to highest degree
    (sig_b, sig_m) = The uncertainties in the coefficients of the fit
    """
    # Fitting the line
    A, fit = fit_poly(x,y,1,no_offset=no_offset)

    # Calculating uncertainties
    sig_y = ( (y - fit)**2 / (y.size - 2) ).sum()**0.5
    sig_b = sig_y * ( (x**2).sum() / (x.size*(x**2).sum() - x.sum()**2) )**0.5
    sig_m = sig_y * ( x.size / (x.size*(x**2).sum() - x.sum()**2) )**0.5

    # Fitting and reporting the chi square
    x2 = ( (y-fit)**2 / np.abs(fit) ).sum()
    p = 1 - chi2.cdf(x2, y.size-2)
    print("x2: {}, p-val: {}\n".format(x2,p))

    return fit, sig_y, A, (sig_b,sig_m)


if __name__ == "__main__":
    # Reading in Hubble's data
    d,v = np.genfromtxt('../data/hubbleoriginal.csv', usecols=(1,2),
                        delimiter=',', dtype=float, skip_header=1).T

    # Fitting a line to it
    v_fit, unc_v, A, unc_A = fit_line(d,v)

    # Reporting the fit
    print("Fitted slope: {}+-{}".format(A[1],unc_A[1]))

    # Plotting the data and fit
    plt.plot(d,v,'ko')
    plt.plot(d, v_fit, color='k', label='Best fit')
    plt.plot(d, A[0]+unc_A[0] + d*(A[1]+unc_A[1]),
             color='r',linestyle=':', label='68% CI')
    plt.plot(d, A[0]-unc_A[0] + d*(A[1]-unc_A[1]),
             color='r',linestyle=':')

    
    # Extra -------------------------------------------------------
    # Fittina a line forced through the origin
    v_fit, unc_v, A, unc_A = fit_line(d,v,no_offset=True)
    
    print("Fitted slope: {}+-{}".format(A[0],unc_A[0]))

    # Plotting it
    plt.plot(d, v_fit, color='g', label='Through origin')
    # -------------------------------------------------------------

    # All the rest of the plotting things
    plt.xlabel('Distance (Mpc)')
    plt.ylabel('Recessional velocity (km/s)')
    plt.title("Fits to Hubble's Original Data")
    plt.legend(loc='upper left')
    plt.savefig('../figures/bordwell_project4A.png')
    
