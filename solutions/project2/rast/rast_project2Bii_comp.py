import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits as pyfits
import scipy.stats as stats
#
plt.ion()
plt.close('all')
#
print 'rast_project2Bii_comp.py'
#
#hdulist=pyfits.open('../../frame-g-000094-1-0131.fits')
hdulist=pyfits.open('frame-g-000094-1-0131.fits')
im=hdulist[0].data
hdulist.close()
#
#csvfilename='../../frame-g-000094-1-0131.csv'
csvfilename='frame-g-000094-1-0131.csv'
csvfile=open(csvfilename,'r')
varname=csvfile.readline().replace('\n','')
csvfile.close()
print('Restoring from '+csvfilename+': '+varname)
#
data=np.genfromtxt(csvfilename,dtype=float,delimiter=',',skip_header=1)
num=data.shape[0]
drows=data[:,1]
dcols=data[:,0]
#
num=data.shape[0]
seed=62659
np.random.seed(seed)
rrows=np.random.uniform(size=(num))*im.shape[1]
rcols=np.random.uniform(size=(num))*im.shape[0]
#
# Calculate nearest neighbor distances in pixels.
dnn=np.zeros(num)
rnn=np.zeros(num)
for n, val in enumerate(dnn):
    dmin=np.sqrt((drows-drows[n])**2+(dcols-dcols[n])**2)
    dmin=dmin[np.where(dmin != 0.0)]
    dnn[n]=np.amin(dmin)
    rmin=np.sqrt((rrows-rrows[n])**2+(rcols-rcols[n])**2)
    rmin=rmin[np.where(rmin != 0.0)]
    rnn[n]=np.amin(rmin)
#
nbins=30
nbins=15
hdnn,binedges=np.histogram(dnn,bins=nbins,range=(0.0,150.0))
hrnn,binedges=np.histogram(rnn,bins=nbins,range=(0.0,150.0))
bincenters=0.5*(binedges[1:]+binedges[:-1])
binwidth=binedges[1]-binedges[0]
#
subfig=plt.subplots(1,2,figsize=(7.5,5.0))
plt.subplot(1,2,1)
plt.bar(bincenters,hdnn/np.float(num),align='center',width=binwidth,color='0.75')
plt.xlim([0.0,150.0])
plt.text(-45.0,0.19,'P(r)',style='italic',fontsize=14)
plt.text(70,-0.04,'r',style='italic',fontsize=14)
plt.title('Star locations',fontsize=14)
#
subfig=plt.subplot(1,2,2)
plt.bar(bincenters,hrnn/np.float(num),align='center',width=binwidth,color='0.75')
plt.xlim([0.0,150.0])
plt.ylim([0.0,0.4])
subfig.yaxis.tick_right()
plt.text(175.,0.19,'P(r)',style='italic',fontsize=14)
plt.text(70,-0.04,'r',style='italic',fontsize=14)
plt.title('Random locations',fontsize=14)
#
plt.subplots_adjust(hspace=0.3)
#
plt.savefig('rast_project2Bii.png',dpi=600,papertype='letter',format='png',bbox_inches='tight')
#
print ' '
print 'Chi-squared test for bin width = '+str(binwidth)+', using only bins with 5 or more counts.'
wh=np.squeeze(np.where(hrnn >= 5.0))
chisqrd=np.sum(np.float64((hdnn[wh]-hrnn[wh])**2)/np.float64(hrnn[wh]))
tmp=stats.chisquare(hdnn[wh],f_exp=hrnn[wh])
print 'Chi-squared value: '+str(chisqrd)
print 'Probability to exceed: '+str(tmp[1])
#
print ' '
print 'Chi-squared test for bin width = '+str(binwidth)+', using only bins with 5 or more counts and excluding first bin (excluding very close neighbors).'
wh=wh[1:]
cntd=np.sum(hdnn[wh])
cntr=np.sum(hrnn[wh])
ratio=np.float64(cntd)/np.float64(cntr)
chisqrd=np.sum(np.float64((hdnn[wh]-hrnn[wh]*ratio)**2)/np.float64(hrnn[wh]*ratio))
tmp=stats.chisquare(hdnn[wh],f_exp=hrnn[wh]*ratio)
print 'Chi-squared value: '+str(chisqrd)
print 'Probability to exceed: '+str(tmp[1])
#
print 'rast_project2Bii_plot.py'
