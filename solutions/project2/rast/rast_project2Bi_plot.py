import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits as pyfits
#
plt.ion()
#
print 'rast_project2Bi_plot.py'
#
plt.close
#
plt.figure(0,figsize=(7.5,9))
plt.subplots_adjust(hspace=0.3)
#
#hdulist=pyfits.open('../../frame-g-000094-1-0131.fits')
hdulist=pyfits.open('frame-g-000094-1-0131.fits')
im=hdulist[0].data
hdulist.close()
#
xmid=1025.0
xmax=2048.0
ra=354.473181333
scale=0.396/60./60.
xticks=np.arange(0,xmax+xmid/2.,xmid/2.0,dtype=float)
xticknames=np.array(scale*(xticks-xmid)+ra,dtype='a7')
ymid=745.0
ymax=1498.0
dec=-0.929952240169
scale=0.396/60./60.
yticks=np.arange(0,ymax,ymid/2.0,dtype=float)
yticknames=np.array(scale*(yticks-ymid)+dec,dtype='a7')
#
#csvfilename='../../frame-g-000094-1-0131.csv'
csvfilename='frame-g-000094-1-0131.csv'
csvfile=open(csvfilename,'r')
varname=csvfile.readline().replace('\n','')
csvfile.close()
print('Restoring from '+csvfilename+': '+varname)
#
data=np.genfromtxt(csvfilename,dtype=float,delimiter=',',skip_header=1)
num=data.shape[0]
#
plt.subplot(2,1,1)
plt.axis([0,xmax,0,ymax])
plt.imshow(0.0*im,cmap='Greys')
plt.scatter(data[:,1],data[:,0],s=16,marker='o',c='black')
plt.xticks(xticks,xticknames)
plt.yticks(yticks,yticknames)
plt.title('Star locations',fontsize=14)
plt.text(xmid-64,-230,'RA',style='italic',fontsize=14)
plt.text(-550,ymid-20,'DEC',style='italic',fontsize=14)
#
num=data.shape[0]
seed=62659
np.random.seed(seed)
rows=np.random.uniform(size=(num))*im.shape[1]
cols=np.random.uniform(size=(num))*im.shape[0]
plt.subplot(2,1,2)
plt.axis([0,xmax,0,ymax])
plt.imshow(0.0*im,cmap='Greys')
plt.scatter(rows,cols,s=16,marker='o',c='black')
plt.xticks(xticks,xticknames)
plt.yticks(yticks,yticknames)
plt.title('Random locations',fontsize=14)
plt.text(xmid-64,-230,'RA',style='italic',fontsize=14)
plt.text(-550,ymid-20,'DEC',style='italic',fontsize=14)
#
plt.savefig('rast_project2Bi.png',dpi=1200,papertype='letter',format='png',bbox_inches='tight')
#
print 'rast_project2Bi_plot.py'
