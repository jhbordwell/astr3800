import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits as pyfits
#
plt.ion()
#
print 'rast_project2Aii_plot.py'
#
plt.close
#
plt.figure(0,figsize=(7.5,9))
plt.subplots_adjust(hspace=0.3)
#
#hdulist=pyfits.open('../../frame-g-000094-1-0131.fits')
hdulist=pyfits.open('frame-g-000094-1-0131.fits')
im=hdulist[0].data
hdulist.close()
#
im[np.where(im <= 0)]=np.amin(im[np.where(im > 0)])
#
xmid=1025.0
xmax=2048.0
ra=354.473181333
scale=0.396/60./60.
xticks=np.arange(0,xmax+xmid/2.,xmid/2.0,dtype=float)
xticknames=np.array(scale*(xticks-xmid)+ra,dtype='a7')
ymid=745.0
ymax=1498.0
dec=-0.929952240169
scale=0.396/60./60.
yticks=np.arange(0,ymax,ymid/2.0,dtype=float)
yticknames=np.array(scale*(yticks-ymid)+dec,dtype='a7')
#
plt.subplot(2,1,1)
plt.axis([0,xmax,0,ymax])
plt.imshow(np.log10(im),cmap='Greys')
plt.xticks(xticks,xticknames)
plt.yticks(yticks,yticknames)
plt.title('Grayscale plot of intensity',fontsize=14)
plt.text(xmid-64,-230,'RA',style='italic',fontsize=14)
plt.text(-550,ymid-20,'DEC',style='italic',fontsize=14)
#
#csvfilename='../../frame-g-000094-1-0131.csv'
csvfilename='frame-g-000094-1-0131.csv'
csvfile=open(csvfilename,'r')
varname=csvfile.readline().replace('\n','')
csvfile.close()
print('Restoring from '+csvfilename+': '+varname)
#
data=np.genfromtxt(csvfilename,dtype=float,delimiter=',',skip_header=1)
#
plt.subplot(2,1,2)
plt.axis([0,xmax,0,ymax])
plt.imshow(0.0*np.log10(im),cmap='Greys')
plt.scatter(data[:,1],data[:,0],s=36,c='black')
plt.xticks(xticks,xticknames)
plt.yticks(yticks,yticknames)
plt.title('Symbol plot of star locations',fontsize=14)
plt.text(xmid-64,-230,'RA',style='italic',fontsize=14)
plt.text(-550,ymid-20,'DEC',style='italic',fontsize=14)
#
plt.savefig('rast_project2Aii.png',dpi=1200,papertype='letter',format='png',bbox_inches='tight')
#
print 'rast_project2Aii_plot.py'
