import numpy as np
from astropy.io import fits

from bordwell_project3Ai_plot import find_files
from saverestore import *


def make_pdf(filename, alt_moments=False):
    """
    This function will make the normed PDF for the on-disk pixels

    INPUT:
    filename = The file to read from
    alt_moments = An optional argument to instead return the
                  mean and variance of the disk pixels
                  calculated directly.

    OUTPUT:
    The PDF values and the left bin edges
    """

    # Reading in the image and getting the on-disk pixels
    img = fits.getdata(filename)
    disk = img[np.where(img > np.median(img)-0.5*np.std(img))]

    # Returning the mean and variance of those pixels if desired
    if alt_moments:
        return disk.mean(), disk.var()

    # Creating the pdf and getting the bin edges
    pdf, bins = np.histogram(disk, bins=1002, normed=True)
    return pdf, bins[:-1]
    

# Collecting the files as in part Ai
red_contrast, Ca_contrast = find_files(['R.P.con', 'K.P.con'])

# Finding and saving the normalized PDFs
red = np.array([ np.vstack( make_pdf(red_contrast[i]) )
                 for i in range(len(red_contrast))]) 
Ca = np.array([ np.vstack( make_pdf(Ca_contrast[i]) )
                for i in range(len(Ca_contrast))]) 

save('../data/bordwell_project3Ci.dat',
     ('red pdfs and bins', 'Ca pdfs and bins'),
     (red, Ca))

# Finding and saving the moments an alternative way
red_alt = np.array([np.vstack(make_pdf(red_contrast[i], alt_moments=True))
                for i in range(len(red_contrast))]).squeeze().T 
Ca_alt = np.array([np.vstack(make_pdf(Ca_contrast[i], alt_moments=True))
               for i in range(len(Ca_contrast))]).squeeze().T 

save('../data/bordwell_project3Cii_altm.dat',
     ('red moments', 'Ca moments'),
     (red_alt, Ca_alt))

