import matplotlib.pyplot as plt
from saverestore import *

# Restoring all the results needed to plot
times, red_data, Ca_data = restore('../data/bordwell_project3Bi.dat')
red_moments, Ca_moments = restore('../data/bordwell_project3Cii.dat')
alt_red, alt_Ca = restore('../data/bordwell_project3Cii_altm.dat')

# Plotting the moments
fig, ax = plt.subplots(2,4,figsize=(20,10))
for i in range(4):
    ax[0,i].plot(times, red_moments[i], marker='o',
                 label='Numerically integrated')
    ax[1,i].plot(times, Ca_moments[i], marker='o',
                 label='Numerically integrated')

    # Adding in the alternatively calculated moments
    if i < 2:
        ax[0,i].plot(times, alt_red[i], marker='o',
                     label = 'Calculated from dist.')
        ax[1,i].plot(times, alt_Ca[i], marker='o',
                     label = 'Calculated from dist.')

    # Labeling
    ax[0,i].xaxis.labelpad = 20
    ax[0,i].set_xlabel('Julian Date [days]')
    ax[0,i].set_ylabel('Moment value')
    ax[0,i].set_title('Red Moment {}'.format(i+1))
    ax[0,i].legend(loc='upper right', fontsize=10)
    
    ax[1,i].xaxis.labelpad = 20
    ax[1,i].set_xlabel('Julian Date [days]')
    ax[1,i].set_ylabel('Moment value')
    ax[1,i].set_title('Ca Moment {}'.format(i+1))
    ax[0,i].legend(loc='upper right', fontsize=10)
    
fig.tight_layout()
fig.savefig('../figures/bordwell_project3Cii.png')
