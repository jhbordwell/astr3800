import numpy as np
from astropy.io import fits
from glob import glob
import matplotlib.pyplot as plt


def find_files(IDs, path='../data/'):
    """
    This function will find all files in the directory specified
    by [path] (default: '../data/') that contain a given substring.

    INPUT:
    IDs = A list of substrings. An array of filenames will 
          be returned for each substring.
    path = An optional input for which directory to find the files in

    OUTPUT:
    A list of arrays of files corresponding to the substrings given.

    """

    files = [np.array(glob(path+'*'+i+'*')) for i in IDs]

    # Files are sorted to ensure alphanumerical order.
    for f in files: f.sort()
    
    return files



if __name__ == "__main__":
    
    def img_and_cut(filename, axis, col):
        """
        This function will plot an image in grayscale, and a 
        cut through the horizontal center of the image.

        INPUT:
        filename = The file to be read in.
        axis = The axis object to plot with (2xn plot expected)
        col = The column to plot in.

        """

        img = fits.getdata(filename)           # Read in data
        axis[0][col].imshow(img, cmap='gray')  # Plotting in 1st row
        axis[0][col].set_xlabel('X [pixels]')
        axis[0][col].set_ylabel('Y [pixels]')

        slice = img[img.shape[0]/2.]
        axis[1][col].plot(slice)               # Plotting in 2nd row
        axis[1][col].set_xlim(0,slice.size)
        axis[1][col].set_xlabel('X [pixels]')
        axis[1][col].set_ylabel('Intensity')

        
    # Finding the files
    red_rdc, red_contrast = find_files(['R.P.rdc','R.P.con'])

    # Creating the 4 panel plot
    fig, ax = plt.subplots(2,2, figsize=(7,7))
    img_and_cut(red_rdc[0], ax, 0)
    img_and_cut(red_contrast[0], ax, 1)

    # Titling the plots with the time
    dirlen = len('../data/')
    timepos = red_rdc[0].index('.',dirlen)
    ax[0][0].set_title('Red Continuum, {}'.format(red_rdc[0][timepos+1: timepos+5]))
    ax[0][1].set_title('Red Contrast, {}'.format(red_rdc[0][timepos+1: timepos+5]))

    fig.tight_layout()
    fig.savefig('../figures/bordwell_project3Ai.png')
