import numpy as np
from astropy.io import fits
import matplotlib.pyplot as plt
from bordwell_project3Ai_plot import find_files

def plot_cut(filename, axis):
    """
    This function will plot the far edge of the disk for
    a file given by [filename] using the axis object [axis]
    """

    # Getting the time of the exposure as HHMM
    dirlen = filename.rindex('/')+1
    timepos = filename.index('.', dirlen)+1
    time = filename[timepos:timepos+4]

    # Grabbing the far edge of the disk
    img = fits.getdata(filename)
    slyce = img[img.shape[0]/2., 1900:1950]

    # Plotting it with exposure time label
    axis.plot((slyce - slyce.min())/slyce.ptp(), label = time)
    
    
# Collecting the files as in part Ai
red_rdc, red_contrast = find_files(['R.P.rdc','R.P.con'])
    
# Plotting the disk edges for each exposure
fig, ax = plt.subplots(1,2,figsize = (16,8))
for i, rdc in enumerate(red_rdc):
    plot_cut(rdc, ax[0])
    plot_cut(red_contrast[i], ax[1])

# Labeling
ax[0].set_title('Red continuum center cuts, edge of disk')
ax[0].set_xlabel('X [pixels]')
ax[0].set_ylabel('Intensity')

ax[1].set_title('Red contrast center cuts, edge of disk')
ax[1].set_xlabel('X [pixels]')
ax[1].set_ylabel('Intensity')

    
# Adding in legends with titles
ax[0].legend(loc = 'upper right', fontsize=10, title='Time of Day')
ax[1].legend(loc = 'upper right', fontsize=10, title='Time of Day')

fig.tight_layout()
fig.savefig('../figures/bordwell_project3Biii.png')
