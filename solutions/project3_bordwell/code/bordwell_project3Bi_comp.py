import numpy as np
from astropy.io import fits

from saverestore import *
from bordwell_project3Ai_plot import find_files


def extract_header_info(filename, *hdr_args):
    """
    Extracts a desired set of header info from a given file

    INPUT:
    filename = The file to read the header from
    hdr_args = A tuple of values to read from the header

    OUTPUT:
    A list of the values corresponding to each header key

    """

    hdr = fits.getheader(filename)
    return [hdr[i] for i in hdr_args]


# Collecting the files as in part Ai
red_rdc, Ca_rdc = find_files(['R.P.rdc','K.P.rdc'])

# Reading in Julian date, AVGWIDTH and RMSA
times = np.array( [extract_header_info(rdc,*['JULDATE'])
                         for rdc in red_rdc], dtype=float).T[0]

red_moments = np.array( [extract_header_info(rdc,*['AVGWIDTH', 'RMSA'])
                         for rdc in red_rdc], dtype=float).T

Ca_moments = np.array( [extract_header_info(rdc,*['AVGWIDTH', 'RMSA'])
                        for rdc in Ca_rdc], dtype=float).T

# Saving the arrays
save('../data/bordwell_project3Bi.dat',
     ('Juldate', 'Red moments', 'Ca moments'),
     (times, red_moments, Ca_moments))


