import numpy as np
from saverestore import *

def find_moment(x, P, num):
    """
    Numerically integrating a PDF [P] along [x] to find
    the [num]th moment.
    """
    return (x**num * P * (x[1]-x[0]) ).sum()


# Restoring my PDFs
red, Ca = restore('../data/bordwell_project3Ci.dat')

# Calculating the first 4 moments
red_moments = np.array([ [find_moment(red[i,1], red[i,0], 1),
                          find_moment(red[i,1], red[i,0], 2),
                          find_moment(red[i,1], red[i,0], 3),
                          find_moment(red[i,1], red[i,0], 4)]
                        for i in range(red.shape[0])]).T
Ca_moments = np.array([ [find_moment(Ca[i,1], Ca[i,0], 1),
                         find_moment(Ca[i,1], Ca[i,0], 2),
                         find_moment(Ca[i,1], Ca[i,0], 3),
                         find_moment(Ca[i,1], Ca[i,0], 4)]
                        for i in range(Ca.shape[0])]).T

# Saving the results
save('../data/bordwell_project3Cii.dat',
     ('Red moments', 'Ca moments'),
     (red_moments, Ca_moments))
     
