import matplotlib.pyplot as plt

from saverestore import *

# Restoring my PDFs for the earliest 3 files
red, Ca = restore('../data/bordwell_project3Ci.dat')

# Prepping to plot each PDF
colors = ['y','r', 'b']
fig, ax = plt.subplots(1,2, figsize=(16, 8))
for i in range(3):
    ax[0].plot(red[i,1], red[i,0], marker='.',label='Image {}'.format(i+1))
    ax[1].plot(Ca[i,1], Ca[i,0], marker='.',label='Image {}'.format(i+1))

# Adding labels and legends    
ax[0].set_xlabel('Intensity')
ax[0].set_title('Red Contrast')
ax[0].legend(loc='upper left')

ax[1].set_xlabel('Intensity')
ax[1].set_title('Ca Contrast')
ax[1].legend(loc='upper right')

fig.tight_layout()
fig.savefig('../figures/bordwell_project3Ci.png')
