import matplotlib.pyplot as plt
from saverestore import *

# Restoring everything needed for the plots
times, red_data, Ca_data = restore('../data/bordwell_project3Bi.dat')
red_moments, Ca_moments = restore('../data/bordwell_project3Cii.dat')

# Plotting the PDF moments versus the avgwidth
fig, ax = plt.subplots(2,2,figsize=(12,12))
for i in range(2):
    ax[i,0].plot(red_data[0], red_moments[i],
                 label='Moment {}'.format(i+1), marker='o',linestyle='None')
    ax[i,1].plot(Ca_data[0], Ca_moments[i],
                 label='Moment {}'.format(i+1), marker='o',linestyle='None')

    # Labeling
    ax[i,0].set_xlabel('AVGWIDTH')
    ax[i,0].set_ylabel('Moment value')
    ax[i,0].set_title('Red Moment {}'.format(i+1))

    ax[i,1].set_xlabel('AVGWIDTH')
    ax[i,1].set_ylabel('Moment value')
    ax[i,1].set_title('Ca Moment {}'.format(i+1))


fig.tight_layout()
fig.savefig('../figures/bordwell_project3Ciii.png')
