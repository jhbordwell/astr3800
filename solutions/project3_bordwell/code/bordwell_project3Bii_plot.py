import matplotlib.pyplot as plt

from saverestore import *
from bordwell_project3Ai_plot import find_files

# Restoring the header values from part Bi
times, red_moments, Ca_moments = restore('../data/bordwell_project3Bi.dat')

# NOTE:
# It's always a good idea to use both different linestyles and different colors,
# because your audience may be looking at your figures in black and white

# Plotting values for each filter against Julian date of the exposure
fig, ax = plt.subplots(2,figsize=(8,8))
ax[0].plot(times, red_moments[0],
           c='r', linestyle=':', label='Red Continuum')
ax[0].plot(times, Ca_moments[0],
           c='b', linestyle='-', label='CaII')
ax[0].legend(loc='center right')
ax[0].set_xlabel('Julian date [days]')
ax[0].set_ylabel('AVGWIDTH')

ax[1].plot(times, red_moments[1],
           c='r', linestyle=':', label='Red Continuum')
ax[1].plot(times, Ca_moments[1],
           c='b', linestyle='-', label='CaII')
ax[1].legend(loc='upper right')
ax[1].set_xlabel('Julian date [days]')
ax[1].set_ylabel('RMSA')

fig.tight_layout()
fig.savefig('../figures/bordwell_project3Bii.png')
