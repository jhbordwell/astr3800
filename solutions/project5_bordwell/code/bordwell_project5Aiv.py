import numpy as np
import matplotlib.pyplot as plt


def timeseries(T,V,ax,title,label, chunk=None):
    """
    Plots a time series, or a subset of a time series.

    Input
    -----
    T, V = Time and velocity axes
    ax = An axis object to receive the plot
    title = The title for the plot
    label = The label for the legend
    chunk = An optional input to plot a subset of T and V

    Output: 
    ------
    (None)
    """

    if chunk != None:
        ax.plot(T[chunk],V[chunk],label=label)
    else:
        ax.plot(T,V,label=label)
    ax.set_xlabel('Time [d]')
    ax.set_ylabel('Velocity [m/s]')
    ax.set_title(title)
    ax.legend()

    
def nearest_neighbor_sampler(x,x_grid,f_grid):
    """
    Samples a series based on nearest neighbors. This basically 
    means that to interpolate between two grid points, the function 
    value corresponding to the closest x-value is chosen.

    Input
    -----
    x = The desired x coordinates
    x_grid, f_grid = The original independent and dependent values

    Output
    ------
    The function values at the nearest neighbor locations.
    """

    # This is the most efficient method if the arrays aren't huge
    #locs = np.abs( x.reshape((1,-1)) - x_grid.reshape((-1,1)) ).argmin(0)

    # This works if they are, finding locations of neighbors
    locs = np.array([np.abs(x_grid - i).argmin() for i in x])
    return f_grid[locs]


def fit_line(x,y):
    """
    Fits a line to a set of independent and dependent values.

    Input
    -----
    x, y = dependent, independent values

    Output
    ------
    The fitted line
    """
    
    X = np.vstack((x,np.ones(x.size,dtype=np.float32)))
    A = np.dot( np.dot(y,X.T), np.linalg.inv(np.dot(X,X.T)) )

    return np.dot(A,X)


# Reading in the data
t, v = np.genfromtxt('../data/SUN_Velocity.txt', delimiter=' ',dtype=float).T
t -= t[0] # shifting the time array to start at t = 0

# Plotting the time series
plt.plot(t,v)
plt.xlabel('Time [d]')
plt.ylabel('Velocity [m/s]')
plt.title('Integrated Doppler velocity of the Solar surface')
plt.savefig('../figures/bordwell_project5Aii.png')

# Creating new, uniform time/velocity arrays
t_unif = np.arange(t.min(),t.max(),(t[1:]-t[:-1]).max(),dtype=np.float32)
v_unif = np.interp(t_unif, t, v)

# Making the four panel plot
fig, ax = plt.subplots(2,2,figsize=(10,10))

# The full time series
timeseries(t,v,ax[0][0],'Full time series','Original')
timeseries(t_unif,v_unif,ax[0][0],'Full time series','Uniform')

# The week of the 11th
timeseries(t,v,ax[0][1],'11th-18th','Original',
           chunk= (t >= 11)*(t<18))
timeseries(t_unif,v_unif,ax[0][1],'11th-18th','Uniform',
           chunk= (t_unif >= 11)*(t_unif<18))

# The first six hours of the 11th
timeseries(t,v,ax[1][0],'First 6h of the 11th','Original',
           chunk= (t >= 11)*(t<11.25))
timeseries(t_unif,v_unif,ax[1][0],'First 6h of the 11th','Uniform',
           chunk= (t_unif >= 11)*(t_unif<11.25))

# The first 1 hour of the 11th
timeseries(t,v,ax[1][1],'First hour of the 11th','Original',
           chunk= (t >= 11)*(t<11+1/24.))
timeseries(t_unif,v_unif,ax[1][1],'First hour of the 11th','Uniform',
           chunk= (t_unif >= 11)*(t_unif<11+1/24.))
for label in ax[1][1].get_xticklabels()[::2]:
        label.set_visible(False)

fig.tight_layout()
fig.savefig('../figures/bordwell_project5Aiv.png')


