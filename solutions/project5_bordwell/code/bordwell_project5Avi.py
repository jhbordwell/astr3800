import numpy as np
import matplotlib.pyplot as plt
from bordwell_project5Aiv import *


# Exploring nearest neighbor sampling
v_nn = nearest_neighbor_sampler(t_unif, t, v)

# Detrending whole data set, and the week of the 11th
v_unif_d = v_unif - fit_line(t_unif,v_unif)

week = (t_unif >= 11)*(t_unif < 18)
v_week_d = v_unif[week] - fit_line(t_unif[week], v_unif[week])
t_week = t_unif[week]

# Making plots to show detrending
fig, ax = plt.subplots(2,2,figsize=(10,7))

# The full time series
timeseries(t_unif,v_unif,ax[0][0],'Full time series','Uniform')
timeseries(t_unif,fit_line(t_unif,v_unif),ax[0][0],'Full time series','Trend')
timeseries(t_unif,v_unif_d,ax[1][0],'Full time series (detrended)','Uniform')

# The week of the 11th
timeseries(t_unif,v_unif,ax[0][1],'11th-18th','Uniform',
           chunk= (t_unif >= 11)*(t_unif<18))
timeseries(t_unif[week],fit_line(t_unif[week],v_unif[week]),ax[0][1],
           '11th-18th','Trend')
timeseries(t_unif[week],v_week_d,ax[1][1],'11th-18th (detrended)','Uniform')

fig.tight_layout()
fig.savefig('../figures/bordwell_project5Avi.png')

