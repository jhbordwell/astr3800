import numpy as np
from numpy.fft import fft
import matplotlib.pyplot as plt
import mpyfit
from scipy.stats import chi2
from bordwell_project5Bi import *
from saverestore import restore
import pdb

def Lorentzian(x,a):
    """
    Returns a Lorentzian profile based on the dependent 
    values and given parameters.

    Input
    -----
    x = dependent values
    a = An array containing the: mean, FWHM, amplitude and offset 

    Output
    ------
    returns the specified Lorentzian profile
    """

    return a[2] * a[1] / ( (x - a[0])**2 + (0.5*a[1])**2 ) + a[3]


def min_Lorentzian(a,args):
    """
    A minimization function for use with mpyfit

    Input
    -----
    a = The parameters of a Lorentzian profile
    args = A tuple of the independent and dependent values

    Output
    ------
    The difference between the specified profile and the data
    """
    
    x,f = args
    return np.abs(Lorentzian(x,a)-f)


def fit_Lorentzian(freq,profile):
    """
    Fits a Lorentzian to a set of data

    Input
    -----
    freq, profile = independent and independent values

    Output
    ------
    The fitted Lorentzian profile
    """

    # Making an initial guess using reasonable properties of the data
    guess = [freq[profile.argmax()], freq.ptp()/2.,
             profile.ptp(), profile.min()]

    # Fitting the data with an offset Lorentzian profile
    parameters, results = mpyfit.fit(min_Lorentzian, guess, (freq,profile))

    return Lorentzian(freq,parameters)


# Plotting power spectra
fig, ax = plt.subplots(1,2, figsize=(10,5))
ax[0].plot(f_unif, ps_unif)
ax[0].set_xlabel('Frequency [mHz]')
ax[0].set_ylabel('Power')
ax[0].set_title('Power spectrum of full time series')
ax[0].set_yscale('log')
ax[0].set_xscale('log')
ax[0].set_xlim(2,7)
ax[0].set_ylim(1e-3,1e6)

ax[1].plot(f_week, ps_week)
ax[1].set_xlabel('Frequency [mHz]')
ax[1].set_ylabel('Power')
ax[1].set_title('Power spectrum of 11th-18th day')
ax[1].set_yscale('log')
ax[1].set_xscale('log')
ax[1].set_xlim(2,7)
ax[1].set_ylim(1e-3,1e6)

fig.tight_layout()
fig.savefig('../figures/bordwell_project5Biii.png')

# Plotting power spectra
fig, ax = plt.subplots(1,2, figsize=(10,5))
ax[0].plot(f_unif, ps_unif)

# Extracting the region via overwrite
f_unif =  f_unif[(f_unif >= 3.25)*(f_unif<3.35)]
ps_unif = ps_unif[(f_unif >= 3.25)*(f_unif<3.35)]

# Fitting a Lorentzian
fit_unif = fit_Lorentzian(f_unif, ps_unif)

ax[0].plot(f_unif, ps_unif)
ax[0].plot(f_unif, fit_unif, color='r')
ax[0].axvline(3.285, color='k', linestyle=':')
ax[0].axvline(3.315, color='k', linestyle=':')
ax[0].set_xlabel('Frequency [mHz]')
ax[0].set_ylabel('Power')
ax[0].set_title('Power spectrum of full time series')
ax[0].set_yscale('log')
ax[0].set_xlim(3.2,3.4)
ax[0].set_ylim(1e-3,1e6)

ax[1].plot(f_week, ps_week)

# Extracting the feature region and fitting a Lorentzian
f_week = f_week[(f_week >= 3.25)*(f_week<3.35)]
ps_week = ps_week[(f_week >= 3.25)*(f_week<3.35)]
fit_week = fit_Lorentzian(f_week, ps_week)

# Plotting the feature and fit
ax[1].plot(f_week, ps_week)
ax[1].plot(f_week, fit_week, color='r')
ax[1].axvline(3.285, color='k', linestyle=':')
ax[1].axvline(3.315, color='k', linestyle=':')
ax[1].set_xlabel('Frequency [mHz]')
ax[1].set_ylabel('Power')
ax[1].set_title('Power spectrum of 11th-18th day')
ax[1].set_yscale('log')
ax[1].set_xlim(3.2,3.4)
ax[1].set_ylim(1e-3,1e6)

fig.tight_layout()
fig.savefig('../figures/bordwell_project5Biv.png')

# Plotting the second feature region
fig, ax = plt.subplots(1,2, figsize=(10,5))
ax[0].plot(f_unif, ps_unif)
ax[0].plot(f_unif[(f_unif>3.3124)*(f_unif<3.325)], ps_unif[(f_unif>3.3124)*(f_unif<3.325)])
ax[0].axvline(3.285, color='k', linestyle=':')
ax[0].axvline(3.315, color='k', linestyle=':')
ax[0].set_xlabel('Frequency [mHz]')
ax[0].set_ylabel('Power')
ax[0].set_title('Power spectrum of full time series')
ax[0].set_yscale('log')
ax[0].set_ylim(1e-3,1e6)

ax[1].plot(f_week, ps_week)
ax[1].plot(f_week[(f_week>3.3124)*(f_week<3.325)], ps_week[(f_week>3.3124)*(f_week<3.325)])
ax[1].axvline(3.285, color='k', linestyle=':')
ax[1].axvline(3.315, color='k', linestyle=':')
ax[1].set_xlabel('Frequency [mHz]')
ax[1].set_ylabel('Power')
ax[1].set_title('Power spectrum of 11th-18th day')
ax[1].set_yscale('log')
ax[1].set_ylim(1e-3,1e6)

fig.tight_layout()
fig.savefig('../figures/bordwell_project5Bvi.png')


# Constructing chi_square measure
cont_unif = ps_unif[(f_unif<3.285)+(f_unif>3.315)]
m_unif, v_unif = cont_unif.mean(), cont_unif.var()
line_unif = ps_unif[(f_unif>=3.285)*(f_unif<3.315)]
chi2_unif = ((line_unif - m_unif)**2).sum() / v_unif
p_unif = 1 - chi2.cdf(chi2_unif, line_unif.size)
print("Full feature: Chi2: {}, p-value: {}".format(chi2_unif, p_unif))

line_unif = ps_unif[(f_unif>=3.3124)*(f_unif<3.325)]
chi2_unif = ((line_unif - m_unif)**2).sum() / v_unif
p_unif = 1 - chi2.cdf(chi2_unif, line_unif.size)
print("Right_feature: Chi2: {}, p-value: {}".format(chi2_unif, p_unif))


cont_week = ps_week[(f_week<3.285)+(f_week>3.315)]
m_week, v_week = cont_week.mean(), cont_week.var()
line_week = ps_week[(f_week>=3.285)*(f_week<3.315)]
chi2_week = ((line_week - m_week)**2).sum() / v_week
p_week = 1 - chi2.cdf(chi2_week, line_week.size)
print("Full feature: Chi2: {}, p-value: {}".format(chi2_week, p_week))

line_week = ps_week[(f_week>=3.3124)*(f_week<3.325)]
chi2_week = ((line_week - m_week)**2).sum() / v_week
p_week = 1 - chi2.cdf(chi2_week, line_week.size)
print("Chi2: {}, p-value: {}".format(chi2_week, p_week))
