import numpy as np
from numpy.fft import fft
import matplotlib.pyplot as plt

from bordwell_project5Avi import *


def power_spectrum(t,v):
    ps = ( np.abs(fft(v))**2 )[:t.size/2.]
    f = np.fft.fftfreq(t.size, t[1]-t[0])[:t.size/2.]
    f /= 24.*3600. / 1000. # converting to mHz
    
    # normalizing
    ps /= ps.size

    return f, ps


# Finding the power spectra and frequency axis
f_unif, ps_unif = power_spectrum(t_unif, v_unif)
f_week, ps_week = power_spectrum(t_week, v_week_d)

# Plotting power spectra
fig, ax = plt.subplots(1,2, figsize=(10,5))
ax[0].plot(f_unif, ps_unif)
ax[0].set_xlabel('Frequency [mHz]')
ax[0].set_ylabel('Power')
ax[0].set_title('Power spectrum of full time series')
ax[0].set_yscale('log')
ax[0].set_xscale('log')
ax[0].set_xlim(1e-4,10)
ax[0].set_ylim(1e-3,1e6)

ax[1].plot(f_week, ps_week)
ax[1].set_xlabel('Frequency [mHz]')
ax[1].set_ylabel('Power')
ax[1].set_title('Power spectrum of 11th-18th day')
ax[1].set_yscale('log')
ax[1].set_xscale('log')
ax[1].set_xlim(1e-4,10)
ax[1].set_ylim(1e-3,1e6)

fig.tight_layout()
fig.savefig('../figures/bordwell_project5Bi.png')
