import numpy as np
import matplotlib.pyplot as plt
from bordwell_project6B import transits
import mpyfit

def Gaussian(x,a):
    """
    Produces a Gaussian for independent values x based 
    on parameters a.

    Input
    -----
    x = independent values
    a = an array containing the mean, standard deviation, 
        amplitude and offset

    Output
    ------
    The corresponding dependent values given the parameters a
    """

    return np.exp(-(x-a[0])**2 / 2 / a[1]**2) * a[2] + a[3]

def min_Gaussian(a,args):
    """
    Finds the difference between a Gaussian with parameters
    array and the data.

    Input
    -----
    a = The Gaussian parameters in the function Gaussian
    args = A tuple containing the independent and dependent
           data values

    Output
    ------
    The absolute difference between the fit and the data
    """

    x, f = args
    return np.abs(Gaussian(x,a)-f)

def fit_Gaussian(x,f):
    """
    This function will fit a Gaussian to the inputs.

    Input
    -----
    x, f = independent and dependent values

    Output
    ------
    -the fitted mean
    -the error in the fit of the mean
    """
    # Setting an initial guess
    guess = [x[f.argmax()], x.ptp()/2., f.ptp(), f.min()]

    # Performing the fit
    parameters, results = mpyfit.fit(min_Gaussian, guess, (x,f))

    return parameters[0], results['parerrors'][0]


def transit_depth(data, u_x, bins=100):
    """
    Finds the K parameter (1-delta) of the transit contained 
    in data by estimating the level of the background and 
    bottom of the transit based on fitting Gaussians to the
    two populations of points.

    Input
    -----
    data = An array containing flux values for a transit observation
    u_x = The uncertainty in the flux values (assumed constant)

    Output
    ------
    K = The K parameter of the transit
    u_K = The propagated uncertainty on the K parameter

    Note
    ----
    This procedure will not work for transits where the 
    transit is comparable to the scatter on the background.
    """
    
    # Making a distribution of the data
    pdf, bins = np.histogram(data,bins=bins)
    bins = bins[:-1] + 0.5*(bins[1]-bins[0]) # Using center of each bin
    
    # Cutting the distribution at the halfway mark
    loc = bins.size/2.

    # Finding the mean value of the bottom of the transit
    dip = bins[:loc], pdf[:loc]
    p1 = fit_Gaussian(*dip)
    u_mean1 = np.sqrt(p1[1]**2 + (u_x/dip[0].size**0.5)**2)

    # Finding the mean value of the continuum
    background = bins[loc:], pdf[loc:]
    p2 = fit_Gaussian(*background)
    u_mean2 = np.sqrt(p2[1]**2 + (u_x/background[0].size**0.5)**2)
    
    # Finding the parameter K
    K = p1[0]/p2[0]
    u_K = np.sqrt( (u_mean1/p2[0])**2 + (u_mean2*p1[0]/p2[0]**2)**2)
    print("K: {}+/-{}".format(K, u_K))

    return K, u_K

# Finding K for each transit
estimates = np.array([list(transit_depth(T[1],T[2][0],bins=100)) for T in transits]).T

# Finding a weighted average of K based on all of the transits
K_avg = np.sum(estimates[0] * 1/estimates[1]**2) / np.sum(1/estimates[1]**2)
u_depth = np.sqrt(1./(1./estimates[1]**2).sum())
print("Transit depth of Wasp4b: {}+/-{}".format(1-K_avg, u_depth))

# Finding the radius of the planet and its uncertainty 
Rsun =  6.963e10 # cm
Rj = 6.9911e9 # cm
Rp = (1-K_avg)**0.5 * (0.87*Rsun) # Rp in cm
u_Rp = Rp*np.sqrt((0.5*u_depth / (1-K_avg))**2 + (0.04/0.87)**2)
print("Radius of Wasp4b (Rj): {}+/-{}".format(Rp/Rj, u_Rp/Rj))

# Finding the density and its uncertainty
Mj = 1.898e30 # g
rho_p = (1.21*Mj) / (4*np.pi/3. * Rp**3) # density in g/cm^3
u_rho = rho_p * np.sqrt((0.12/1.21)**2 + (3*u_Rp/Rp)**2)
print("Density of Wasp4b: {}+/-{}".format(rho_p, u_rho))
