import numpy as np
import matplotlib.pyplot as plt

# Reading in data
t, f, uf = np.genfromtxt('../data/wasp_4b.tsv', delimiter=';', dtype=float, skip_header=36).T

# Getting the timesteps in between each point to allow me to split up the datasets
dt = t[1:]-t[:-1]  ;  locs = [0]

# The half-sigma cutoff was picked through trial and error,
# one-sigma almost works but misses one transit
for i in np.where(dt > dt.mean()+dt.std()*0.5)[0]: locs.append(i)
locs.append(-1)

# Another way to find the transit cutoffs, if you knew there were 6 transits:
# locs += list(np.argsort(dt)[-5:])
# locs.append(-1)

# Splitting up the transits
transits = [[ t[locs[i]:locs[i+1]],
              f[locs[i]:locs[i+1]],
              uf[locs[i]:locs[i+1]]] for i in range(len(locs)-1)]

# Removing the time offset, and cutting off the first point (often bad)
transits = [[ T[0][1:] - T[0][1], T[1][1:], T[2][1:] ] for T in transits]

# Displaying the 6 individual transits
fig, ax = plt.subplots(2,3,figsize=(12,6))
for i,T in enumerate(transits):
    ax[i/3][i%3].plot(T[0],T[1])
    ax[i/3][i%3].set_xlabel('Time  [days]')
    ax[i/3][i%3].set_ylabel('DF/F')
    ax[i/3][i%3].set_title('Transit {}'.format(i+1))
    ax[i/3][i%3].set_xticks(ax[i/3][i%3].get_xticks()[::2])
fig.tight_layout()
fig.savefig('../figures/bordwell_project6Aii.png')
plt.close()

# Displaying the 6 transits all together
for T in transits: plt.plot(T[0],T[1],marker='o', label='Transit {}'.format(i+1))
plt.xlabel('Time [days]')
plt.ylabel('DF/F')
plt.legend(loc = 'lower right')
plt.title('6 Transits of Wasp 4b')
plt.savefig('../figures/bordwell_project6Aiv.png')
    
