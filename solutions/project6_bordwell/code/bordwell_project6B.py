import numpy as np
import matplotlib.pyplot as plt
from bordwell_project6A import transits

def cross_correlation(f,g):
    """
    This function will cross-correlate the functions f and g, 
    and return the shift which maximizes the cross-correlation

    INPUT
    -----
    f,g = the two datasets to cross-correlate

    OUTPUT
    ------
    The shift of f at which the cross correlation was maximized
    """
    return np.array([np.sum(g*np.roll(f,-step)) for step in range(f.size)]).argmax()
        

# Taking the reference transit out of the transit list
ref = transits.pop(1)

# Finding the time shifts which maximize alignment of the two transits
shifts = []  ;  shifts2 = []
for T in transits:
    T_interp = np.interp(ref[0],T[0],T[1]) # Interpolating to the reference time

    # using cross-correlation in physical space
    shifts.append(ref[0][cross_correlation(T_interp, ref[1])])

    # using the correlation theorem
    shifts2.append(ref[0][np.fft.ifft(np.fft.fft(T_interp) * np.conj(np.fft.fft(ref[1]))).argmax()])

print("The shifts between transits and the reference were found to be:")
for i in range(len(transits)): print("{}, {}".format(shifts[i], shifts2[i]))

# Modifying the transits list for the next part
transits = [[T[0]-shifts[i],T[1],T[2]] for i,T in enumerate(transits)]
transits.insert(1,ref)

# Displaying the 6 transits all together
plt.close()
for T in transits: plt.plot(T[0],T[1],marker='o',label='Transit {}'.format(i+1))
plt.xlabel('Time [days]')
plt.ylabel('DF/F')
plt.legend(loc='lower right')
plt.title('6 Transits of Wasp 4b')
plt.savefig('../figures/bordwell_project6B.png')

                
