import numpy as np
#
print 'cointoss.py'
#
numelements=10
seed=62659
coin=np.chararray(numelements) #set array size
coin[:]='T' #initialize the array
#
#If you want to read the seed for the random number generator from the terminal rather than hard code it (above) you would use the next two lines.  Hard codeing the seed is good for code reproducability, but is inflexible. 
#print 'Seed?'
#seed=input()
#
#The next five lines of code yield one numelements long string of heads or tails
#np.random.seed(seed)
#rand=np.random.uniform(size=(numelements))
#coin[np.where(rand < 0.5)]='H'
#print coin
#
#The code below iterates many random number sequences until arriving at the specified H,T,H,T,etc. sequence, and counts how many trials it took.
n=0
np.random.seed(seed)
#The two while statements below perform identically and the syntax choice is stylistic.
#while (coin != ['H','T','H','T','H','T','H','T','H','T']).any():
while any(coin != ['H','T','H','T','H','T','H','T','H','T']):
    coin[:]='T'
    rand=np.random.uniform(size=(numelements))
    coin[np.where(rand < 0.5)]='H'
    n=n+1
    print n
    print coin
#
print 'cointoss.py'
